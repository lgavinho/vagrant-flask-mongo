# Python Flask Development Environment with Ubuntu #

* Apache2
* Python
* MongoDB

## Install ##

* Vagrant - [http://www.vagrantup.com/](http://www.vagrantup.com/)
* Virtualbox - [https://www.virtualbox.org/](https://www.virtualbox.org/) 

Then run...

```
#!bash
git clone git@bitbucket.org:lgavinho/vagrant-flask-mongo.git
cd vagrant-flask-mongo
vagrant up

```

## How to use ##

* Add to your hosts


```
#!text

192.168.33.10   python.dev

```

* Open in your browser http://python.dev